<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'index_controller';
$route['index'] = 'index_controller';
$route['login'] = "login_controller";
$route['login/([a-zA-Z]+)'] = "login_controller/$1";
$route['gerenciamento/([a-zA-Z]+)'] = "$1_controller";
$route['gerenciamento/([a-zA-Z]+)/adicionar'] = "$1_controller/manage/add";
$route['gerenciamento/([a-zA-Z]+)/editar/(:num)'] = "$1_controller/manage/edit/$2";
$route['gerenciamento/([a-zA-Z]+)/deletar/(:any)'] = "$1_controller/delete/$2";
$route['gerenciamento/([a-zA-Z]+)/(:any)'] = "$1_controller/$2";
$route['gerenciamento/([a-zA-Z]+)/(:any)/(:any)'] = "$1_controller/$2/$3";
$route['procurar-advogado'] = "search_controller";
$route['procurar-advogado/(:num)/(:num)'] = "search_controller/index/$1/$2";
$route['advogado/(:num)/(:any)'] = "search_controller/lawyer/$1/$2";
/*
$route['([a-zA-Z]+)/([a-zA-Z0-9]+)'] = "$1_controller/$2";
$route['gerenciamento/([a-zA-Z0-9]+)'] = "$1_controller/$2";
$route['([a-zA-Z]+)/([a-zA-Z]+)/([a-zA-Z0-9]+)'] = "$1_controller/$2/$3";*/
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
