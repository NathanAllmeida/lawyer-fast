<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_controller extends CI_Controller {
    
    private $level = 0;
    
	public function index()	{

		if($this->login->ValidationPermissions($this->level)){
			$usuario = $this->session->userdata('logged');
			$perfil = $usuario['perfil'];			
			if ($perfil==4) {								
				$this->template->write_view('menu','adm/menu',$usuario,FALSE);
				$this->template->write_view('header','adm/header',$usuario,FALSE);
				$this->template->write_view('content','adm/dashboard',$usuario,FALSE);
				$this->template->render();
			}elseif($perfil==3){
				$this->template->write_view('menu','man/menu',$usuario,FALSE);				
				$this->template->write_view('header','man/header',$usuario,FALSE);
				$this->template->write_view('content','man/dashboard',$usuario,FALSE);
				$this->template->render();
			}elseif($perfil==2){
				$this->template->write_view('menu','adv/menu',$usuario,FALSE);	
				$this->template->write_view('header','adv/header',$usuario,FALSE);		
				$this->template->write_view('content','adv/dashboard',$usuario,FALSE);
				$this->template->render();
			}elseif($perfil==1){
				$this->template->write_view('menu','user/menu',$usuario,FALSE);
				$this->template->write_view('header','user/header',$usuario,FALSE);						
				$this->template->write_view('content','user/dashboard',$usuario,FALSE);
				$this->template->render();
			}else{
				redirect('403');
			}
		}else{
			if ($this->session->userdata('logged')) {
				redirect('403');
			}else{
				redirect('login');
			}
		}
    }

    function __construct(){
        parent::__construct();
		if ($this->login->ValidationPermissions($this->level)){
			$this->load->model('core_model');
			$this->load->model('advogado_model');			
		}else{			
			if ($this->session->userdata('logged')) {
				if($_SERVER['HTTP_X_REQUESTED_WITH']){
					$this->output->set_status_header(403);
					echo json_encode(array('error'=>'Não Autorizado!','msg'=>'Você não tem acesso a esse página!'));
					exit();
				}else{					
					redirect('index');
				}
			}else{						
				if($_SERVER['HTTP_X_REQUESTED_WITH']){
					$this->output->set_status_header(401);
					echo json_encode(array('error'=>'Não Autorizado!','msg'=>'Faça login!'));
					exit();
				}else{
					redirect('login');
				}
			}
        }
    }

	public function ListAll(){					
		$data = $this->core_model->ListAll();						
		echo json_encode(array('data'=>$data));
    }
    public function ListById(){					
        $usuario = $this->input->post('usuario');		
		$data = $this->core_model->ListById('login');						
		echo json_encode(array('data'=>$data));
	}

	public function Add(){		
		$nome = $this->input->post('nome');		
		$dirigente = $this->input->post('dirigente');		
		$ajudante = $this->input->post('ajudante');		

		$data_i = array(
			'nome' => $nome,			
			'dirigente' => $dirigente,
			'ajudante' => $ajudante
		);		

		$this->load->model('core_model');
		$result_i = $this->core_model->Adiciona($data_i);

		if ($result_i) {
			echo json_encode(array('result'=>'success'));
		}else{
			echo json_encode(array('result'=>'error'));
		}

	}

	public function Alterar(){
		$id = $this->input->post('idgrupo');
		$nome = $this->input->post('nome');		
		$dirigente = $this->input->post('dirigente');			
		$ajudante = $this->input->post('ajudante');		

		$data_u = array(
			'nome' => $nome,			
			'dirigente' => $dirigente,
			'ajudante' => $ajudante
		);		

		$this->load->model('core_model');
		$result_u = $this->core_model->Altera($data_u,$id);

		if ($result_u) {
			echo json_encode(array('result'=>'success'));
		}else{
			echo json_encode(array('result'=>'error'));

		}
	}
	public function Deletar(){
		$id = $this->input->post('idgrupo');

		$this->load->model('core_model');
		$data_d = array(
			'idgrupo' => $id,			
		);	
		$result_d = $this->core_model->Deleta($data_d);

		if ($result_d) {
			echo json_encode(array('result'=>'success'));
		}else{
			echo json_encode(array('result'=>'error'));

		}
	}
}
