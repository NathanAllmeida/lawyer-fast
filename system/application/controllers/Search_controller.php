<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_controller extends CI_Controller {
    
    private $level = 0;
    
	public function index($type = null,$id = null)	{

		if($this->login->ValidationPermissions($this->level)){		
			$data = $this->session->userdata('logged');					
			$this->search($data,$type,$id);			
		}else{
			if ($this->session->userdata('logged')) {
				redirect('403');
			}else{
				redirect('login');
			}
		}
    }

    function __construct(){
		parent::__construct();
		if ($this->login->ValidationPermissions($this->level)){
			$this->load->model('core_model');
			$this->load->model('advogado_model');			
			$this->load->library('GoogleApi');
			$this->load->helper('search');
			$this->load->helper('lawyer');
		}else{			
			if ($this->session->userdata('logged')) {
				if($_SERVER['HTTP_X_REQUESTED_WITH']){
					$this->output->set_status_header(403);
					echo json_encode(array('error'=>'Não Autorizado!','msg'=>'Você não tem acesso a esse página!'));
					exit();
				}else{
					redirect('index');
				}
			}else{						
				if($_SERVER['HTTP_X_REQUESTED_WITH']){
					$this->output->set_status_header(401);
					echo json_encode(array('error'=>'Não Autorizado!','msg'=>'Faça login!'));
					exit();
				}else{
					redirect('login');
				}
			}
        }  
    }	
	public function search($data = array(), $type = null,$id = null){
		$this->load->helper('tipos');
		
		$data['tipos'] = getTipos($this->db);
		$data['step'] = 1;
		$data['localizacao'] = 0;
		$data['advogados'] = array();		
		if($type!=null&&$type = 3&&$id!=null){
			$data['step'] = 3;			
			$this->db->where('id = '.$id);
			$result = $this->core_model->listAll('busca',false);
			$this->session->set_userdata('url_last_search',base_url('procurar-advogado/3/'.$id));
			$location = $this->googleapi->getGeocoding($result[0]['cep']);			
			doSearchLawyer($location,$this->db);
			$result = $this->core_model->listAll('advogado');	
			$data['advogados'] = prepareDataLawyer($result);			
		
		}

		$this->template->write_view('menu','user/menu',$data,FALSE);
		$this->template->write_view('header','user/header',$data,FALSE);
		$this->template->write_view('content','user/search',$data,FALSE);
		$this->template->render();
	}
	public function lawyer($id = null, $name = null){
		if($id!=null&&$name!=null){
			$data = array();
			$this->db->where("id_usuario = ".$id);
			$result = $this->core_model->listAll('advogado');
			if(count($result)>0){
				$result = prepareDataLawyer($result);
				if($result[0]['name_url']==$name){
					$data['advogado'] = $result[0];
					$data['url_search'] = $this->session->userdata('url_last_search');
					$this->template->write_view('menu','user/menu',$data,FALSE);
					$this->template->write_view('header','user/header',$data,FALSE);
					$this->template->write_view('content','adv/public_profile',$data,FALSE);
					$this->template->render();
				}else{
					redirect('404');
				}
			}else{
				redirect('404');
			}
		}else{
			redirect('404');
		}
	}

	public function listAll($type = null){	
		if($type != null){
			switch ($type) {
				case 'pending-oabs':
					$this->db->where('advogado.status_oab = 0');
					break;
				
				default:
					# code...
					break;
			}
		}	
		$this->db->where('advogado.ativo = 1');
		$this->db->where('advogado.deleted = 0');
		$this->db->where('login.perfil = 2');
		$this->db->select('*,advogado.ativo as ad_ativo, advogado.deleted as ad_deleted');
		$data = $this->core_model->ListAllWithMultiple(array('usuario','advogado','login'),array('id_usuario','id_usuario','id_usuario'),'INNER',FALSE);		
		foreach ($data as $i=>$row) {
			$data[$i]['ativo'] = $row['ativo'] == 0? 'Inativo' : 'Ativo';
			$data[$i]['ativo_2'] = $row['ativo'];

			$data[$i]['ad_ativo'] = $row['ad_ativo'] == 0? 'Inativo' : 'Ativo';
			$data[$i]['ad_ativo'] = $row['ad_ativo'];
			if($row['status_oab'] == 1){
				$data[$i]['status_oab'] = 'Verificado';
			}else if($row['status_oab'] == 2){
				$data[$i]['status_oab'] = 'Erro';
			}else{
				$data[$i]['status_oab'] = 'Não verificado';
			}
			$data[$i]['status_oab_2'] = $row['status_oab'];			
			$data[$i]['url_edit'] = base_url('gerenciamento/advogado/editar/'.$data[$i]['id_usuario']);
			$data[$i]['url_verify'] = base_url('gerenciamento/advogado/verify_oab/'.sha1(md5($data[$i]['id_usuario'])));
			$data[$i]['url_delete'] = base_url('gerenciamento/advogado/deletar/'.sha1(md5($data[$i]['id_usuario'])));
		}
		echo json_encode(array('data'=>$data));
	}

	public function manage($type = null, $id_usuario = null){
		$data = array();
		if($type!==null){
			$data['type'] = $type;
			$data['url_back'] = base_url('gerenciamento/advogado');
			if($id_usuario!==null){
				$this->db->where(array('usuario.id_usuario'=>$id_usuario));
				$result = $this->core_model->ListAllWith('usuario','login','id_usuario','id_usuario','INNER',FALSE);
				if(count($result)>0&&!empty($result)){
					$data['usuario'] = $result[0];
				}else{
					$data['usuario'] = array();
				}
				$this->template->write_view('menu','adm/menu',$data,FALSE);
				$this->template->write_view('header','adm/header',$data,FALSE);
				$this->template->write_view('content','usuarios/manage',$data,FALSE);
				$this->template->render();
			}else{
				$data['usuario'] = array();
				$this->template->write_view('menu','adm/menu',$data,FALSE);
				$this->template->write_view('header','adm/header',$data,FALSE);
				$this->template->write_view('content','usuarios/manage',$data,FALSE);
				$this->template->render();
			}
		}else{
		}
	}
	
	public function Insert(){		
		$id_usuario = $this->session->userdata('logged')['id_usuario'];
		$tipo = $this->input->post('tipo');		
		$cep = $this->input->post('cep');		
		$detalhes = $this->input->post('detalhes');
		$endereco = $this->input->post('endereco');

		$data_i_b = array(
			'id_usuario' => $id_usuario,			
			'id_tipo' => $tipo,
			'cep' => $cep,
			'detalhes' => $detalhes,		
			'endereco' => $endereco		
		);		

		$this->load->model('core_model');
		$result_i_b = $this->core_model->Add('busca',$data_i_b);

		if ($result_i_b) {					
			$this->output->set_status_header(200);
			echo json_encode(array('result'=>'success','msg'=>'Busca adicionada com sucesso!','redirect'=>true,'url'=>base_url('procurar-advogado/3/'.$this->core_model->LastId())));
		}else{
			$this->output->set_status_header(500);
			echo json_encode(array('result'=>'error','msg'=>'Erro ao adicionar busca, tente novamente, caso persista entre em contato com o Administrador!'));
		}

	}

	public function Delete(){
		if($this->uri->segment(4)===null){
			$this->output->set_status_header(400);
			echo json_encode(array('result'=>'error','msg'=>'Erro ao deletar advogado! URL Invalida!'));
		}else{
			$id_usuario = $this->uri->segment(4);		

			$this->load->model('core_model');
			$data_d = array(
				'sha1(md5(id_usuario))' => $id_usuario,			
			);	

			$verify = $this->core_model->ListByField('advogado',$data_d);			
			if(count($verify)>0){

				$result_d = $this->core_model->Delete('advogado',$data_d);	
				if ($result_d) {
					$data_u_l = array(
						'perfil'=>1
					);
					$result_u_l = $this->core_model->Edit('login',$data_u_l,$data_d);
					if($result_u_l){
						$this->output->set_status_header(200);
						echo json_encode(array('result'=>'success','msg'=>'Usuário deletado com sucesso!'));
					}else{
						$this->output->set_status_header(500);
						echo json_encode(array('result'=>'error','msg'=>'Erro ao atualizar login, tente novamente, caso persista entre em contato com o Administrador!'));		
					}
					
				}else{			
					$this->output->set_status_header(500);
					echo json_encode(array('result'=>'error','msg'=>'Erro ao deletar usuário, tente novamente, caso persista entre em contato com o Administrador!'));		
				}
			}else{
				$this->output->set_status_header(400);
				echo json_encode(array('result'=>'error','msg'=>'Erro ao deletar usuário! URL Invalida!'));
			}
		}

	}

	public function get_numbers(){
		$data = array();

		$this->db->where('advogado.ativo = 1');
		$this->db->where('advogado.deleted = 0');
		$this->db->where('login.perfil = 2');
		$this->db->select('count(*)');
		$result_registered = $this->core_model->ListAllWithMultiple(array('usuario','advogado','login'),array('id_usuario','id_usuario','id_usuario'),'INNER',FALSE)[0];
		$data['registered'] = $result_registered['count(*)'];

		$this->db->where('advogado.ativo = 1');
		$this->db->where('advogado.deleted = 0');
		$this->db->where('login.perfil = 2');
		$this->db->select('count(*)');
		$this->db->where("last_login >= '".date("Y-m-d", strtotime("first day of previous month"))."'");
		$result_access_last_month = $this->core_model->ListAllWithMultiple(array('usuario','advogado','login'),array('id_usuario','id_usuario','id_usuario'),'INNER',FALSE)[0];
		$data['access_last_month'] = $result_access_last_month['count(*)'];
		
		$this->db->where('advogado.ativo = 1');
		$this->db->where('advogado.deleted = 0');
		$this->db->where('advogado.status_oab = 1');	
		$this->db->where('login.perfil = 2');
		$this->db->select('count(*)');
		$this->db->where("last_login >= '".date("Y-m-d", strtotime("first day of previous month"))."'");
		$result_oabs_verified = $this->core_model->ListAllWithMultiple(array('usuario','advogado','login'),array('id_usuario','id_usuario','id_usuario'),'INNER',FALSE)[0];
		$data['oabs_verified'] = $result_oabs_verified['count(*)'];

		$this->db->where('advogado.ativo = 1');
		$this->db->where('advogado.deleted = 0');
		$this->db->where('advogado.status_oab = 0');
		$this->db->where('login.perfil = 2');
		$this->db->select('count(*)');
		$this->db->where("last_login >= '".date("Y-m-d", strtotime("first day of previous month"))."'");
		$result_oabs_pending = $this->core_model->ListAllWithMultiple(array('usuario','advogado','login'),array('id_usuario','id_usuario','id_usuario'),'INNER',FALSE)[0];
		$data['oabs_pending'] = $result_oabs_pending['count(*)'];

		return $data;
	}

	public function verify_oab(){
		if($this->uri->segment(4)===null){
			$this->output->set_status_header(400);
			echo json_encode(array('result'=>'error','msg'=>'Erro ao aprovar advogado! URL Invalida!'));
		}else{
			$id_usuario = $this->uri->segment(4);		

			$this->load->model('core_model');
			$data_d = array(
				'sha1(md5(id_usuario))' => $id_usuario,			
			);	

			$verify = $this->core_model->ListByField('advogado',$data_d);			
			if(count($verify)>0){
				$result_d = $this->core_model->Edit('advogado',array('status_oab'=>1),$data_d);	
				if ($result_d) {					
					$this->output->set_status_header(200);
					echo json_encode(array('result'=>'success','msg'=>'Advogado aprovado com sucesso!'));									
				}else{			
					$this->output->set_status_header(500);
					echo json_encode(array('result'=>'error','msg'=>'Erro ao aprovar advogado, tente novamente, caso persista entre em contato com o Administrador!'));		
				}
			}else{
				$this->output->set_status_header(400);
				echo json_encode(array('result'=>'error','msg'=>'Erro ao aprovar advogado! URL Invalida!'));
			}
		}
	}

}
