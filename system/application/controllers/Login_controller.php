<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_controller extends CI_Controller {

	public function index()	{
        if ($this->session->userdata('logged')) {
			redirect('index_controller');
		}else{
            $this->load->view('login');   
        }
    }

    function __construct(){
        parent::__construct();
        $this->load->model('core_model');
        $this->load->model('login_model');         
    }

	public function Authentication(){        
        $email = $this->input->post('email');
        $pass = md5($this->input->post('pass'));
        if($email == "" or $email == " " or  $pass == "" or $pass == " "){
            $this->output->set_status_header(200);
            echo json_encode(array('result'=>'error','msg'=>'Preencha os campos!'));           
        } else{
            $result = $this->login_model->SelectUserWithPass($email,$pass);						
            if(count($result)>0){
                $session = array(
                    'id_usuario'=>$result[0]['id_usuario'],
                    'nome'=>$result[0]['nome'],
                    'email'=>$result[0]['email'],
                    'perfil'=>(int)$result[0]['perfil']                    
                );
                $this->session->set_userdata('logged',$session);
                $this->core_model->edit('login',array('last_login'=>date('Y-m-d H:i:s')),array('id_usuario',$session['id_usuario']));
                $this->output->set_status_header(200);
                echo json_encode(array('result'=>'success','url'=>base_url()));
            }else{
                $this->output->set_status_header(200);
                echo json_encode(array('result'=>'error','msg'=>'Email ou Senha Errados!'));
            }
        }
		
    }
   
    public function ValidationPermissions($level = null){	
        
        if ($this->session->userdata('logged')) {
            if($level === null){
                $perfil = $this->ValidationLogin()['perfil'];
                if($perfil == 1 && ($level == 1 or $level == 0)){// Se o perfil for igual a usuario e o nivel de acesso for igual a usuario, permite acesso.
                    return true;
                }else if($perfil == 2 && ($level == 2 or $level == 0)){//Se o perfil for igual a advogado e o nivel de acesso for igual a advogado, permite acesso.
                    return true;
                }else if((($perfil != 1)&&($perfil != 2)) && $perfil >= $level){//Se o perfil for diferente de advogado e usuario e for maior que o nivel de acesso permite o acesso
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }		
    }
}
