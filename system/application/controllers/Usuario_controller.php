<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_controller extends CI_Controller {
    
    private $level = 0;
    
	public function index()	{

		if($this->login->ValidationPermissions($this->level)){
			$usuario = $this->session->userdata('logged');
			$perfil = $usuario['perfil'];			
			if ($perfil==4) {
				$this->template->write_view('menu','adm/menu',$usuario,FALSE);
				$this->template->write_view('header','adm/header',$usuario,FALSE);
				$this->template->write_view('content','usuarios/list',$usuario,FALSE);
				$this->template->render();
			}elseif($perfil==3){
				$this->template->write_view('menu','man/menu',$usuario,FALSE);				
				$this->template->write_view('header','man/header',$usuario,FALSE);
				$this->template->write_view('content','man/usuario',$usuario,FALSE);
				$this->template->render();
			}elseif($perfil==2){
				$this->template->write_view('menu','adv/menu',$usuario,FALSE);	
				$this->template->write_view('header','adv/header',$usuario,FALSE);		
				$this->template->write_view('content','adv/usuario',$usuario,FALSE);
				$this->template->render();
			}elseif($perfil==1){
				$this->template->write_view('menu','user/menu',$usuario,FALSE);
				$this->template->write_view('header','user/header',$usuario,FALSE);						
				$this->template->write_view('content','user/usuario',$usuario,FALSE);
				$this->template->render();
			}else{
				redirect('403');
			}
		}else{
			if ($this->session->userdata('logged')) {
				redirect('403');
			}else{
				redirect('login');
			}
		}
    }

    function __construct(){
		parent::__construct();
		if ($this->login->ValidationPermissions($this->level)){
			$this->load->model('core_model');
			$this->load->model('usuario_model');			
		}else{			
			if ($this->session->userdata('logged')) {
				if($_SERVER['HTTP_X_REQUESTED_WITH']){
					$this->output->set_status_header(403);
					echo json_encode(array('error'=>'Não Autorizado!','msg'=>'Você não tem acesso a esse página!'));
					exit();
				}else{
					redirect('index');
				}
			}else{						
				if($_SERVER['HTTP_X_REQUESTED_WITH']){
					$this->output->set_status_header(401);
					echo json_encode(array('error'=>'Não Autorizado!','msg'=>'Faça login!'));
					exit();
				}else{
					redirect('login');
				}
			}
        }  
    }

	public function listAll(){		
		$data = $this->core_model->ListAllWith('usuario','login','id_usuario','id_usuario','INNER',FALSE);
		foreach ($data as $i=>$row) {
			$data[$i]['ativo'] = $row['ativo'] == 0? 'Inativo' : 'Ativo';
			$data[$i]['ativo_2'] = $row['ativo'];
			$data[$i]['last_login'] = date('d/m/Y H:i:s',strtotime($row['last_login']));
			$data[$i]['last_login_2'] = $row['last_login'];
			$data[$i]['url_edit'] = base_url('gerenciamento/usuario/editar/'.$data[$i]['id_usuario']);
			$data[$i]['url_delete'] = base_url('gerenciamento/usuario/deletar/'.sha1(md5($data[$i]['id_usuario'])));
		}
		echo json_encode(array('data'=>$data));
	}

	public function manage($type = null, $id_usuario = null){
		$data = array();
		if($type!==null){
			$data['type'] = $type;
			if($id_usuario!==null){
				$this->db->where(array('login.id_usuario'=>$id_usuario));
				$result_login = $this->core_model->ListByField('login',array('id_usuario'=>$id_usuario),false);
				
				$this->db->where(array('usuario.id_usuario'=>$id_usuario));
				if($result_login[0]['perfil']==2){					
					$result = $this->core_model->ListAllWithMultiple(array('usuario','advogado','login'),array('id_usuario','id_usuario','id_usuario'),'INNER',FALSE);
				}else{
					$result = $this->core_model->ListAllWith('usuario','login','id_usuario','id_usuario','INNER',FALSE);
				}				
				if(count($result)>0&&!empty($result)){
					$data['usuario'] = $result[0];
				}else{
					$data['usuario'] = array();
				}
				
				$this->template->write_view('menu','adm/menu',$data,FALSE);
				$this->template->write_view('header','adm/header',$data,FALSE);
				$this->template->write_view('content','usuarios/manage',$data,FALSE);
				$this->template->render();
			}else{
				$data['usuario'] = array();
				$this->template->write_view('menu','adm/menu',$data,FALSE);
				$this->template->write_view('header','adm/header',$data,FALSE);
				$this->template->write_view('content','usuarios/manage',$data,FALSE);
				$this->template->render();
			}
		}else{
		}
	}
	public function Insert(){		
		$nome = $this->input->post('nome');		
		$email = $this->input->post('email');		
		$data_nascimento = $this->input->post('data_nascimento');		
		$telefone = $this->input->post('telefone');		
		$cpf = $this->input->post('cpf');		
		$senha = md5($this->input->post('senha'));		
		$perfil = $this->input->post('perfil');

		$oab = ($this->input->post('oab')!==null&&!empty($this->input->post('oab')))?$this->input->post('oab'):'';
		$rua = ($this->input->post('rua')!==null&&!empty($this->input->post('rua')))?$this->input->post('rua'):'';
		$numero = ($this->input->post('numero')!==null&&!empty($this->input->post('numero')))?$this->input->post('numero'):'';
		$bairro = ($this->input->post('bairro')!==null&&!empty($this->input->post('bairro')))?$this->input->post('bairro'):'';
		$complemento = ($this->input->post('complemento')!==null&&!empty($this->input->post('complemento')))?$this->input->post('complemento'):'';
		$cep = ($this->input->post('cep')!==null&&!empty($this->input->post('cep')))?$this->input->post('cep'):'';
		$cidade = ($this->input->post('cidade')!==null&&!empty($this->input->post('cidade')))?$this->input->post('cidade'):'';
		$estado = ($this->input->post('estado')!==null&&!empty($this->input->post('estado')))?$this->input->post('estado'):'';
		$nome_comercial = ($this->input->post('nome_comercial')!==null&&!empty($this->input->post('nome_comercial')))?$this->input->post('nome_comercial'):'';
		$descricao = ($this->input->post('descricao')!==null&&!empty($this->input->post('descricao')))?$this->input->post('descricao'):'';

		$data_i_u = array(
			'nome' => $nome,			
			'email' => $email,
			'data_nascimento' => $data_nascimento,
			'telefone' => $telefone,
			'cpf' => $cpf,			
			'ativo' => '1',			
			'deleted' => '0',			
			'createdAt' => date('Y-m-d H:i:s'),			
			'updatedAt' => date('Y-m-d H:i:s')			
		);		

		$this->load->model('core_model');
		$result_i_u = $this->core_model->Add('usuario',$data_i_u);

		if ($result_i_u) {	
			$result_id = $this->core_model->LastId();
			$data_i_l = array(
				'id_usuario'=>$result_id,
				'senha' => $senha,			
				'perfil' => $perfil,		
				'last_login' => date('Y-m-d H:i:s'),			
				'updatedAt' => date('Y-m-d H:i:s')			
			);	
			$result_i_l = $this->core_model->Add('login',$data_i_l);
			if($result_i_l){
				if($perfil==2){
					$data_u_a = array(
						'oab'=>$oab,
						'rua'=>$rua,
						'numero'=>$numero,
						'bairro'=>$bairro,
						'complemento'=>$complemento,
						'cep'=>$cep,
						'cidade'=>$cidade,
						'estado'=>$estado,
						'nome_comercial'=>$nome_comercial,
						'descricao'=>$descricao
					);
								
					$data_u_a['id_usuario'] = $result_id;
					$result_u_a = $this->core_model->Add('advogado',$data_u_a);					

					if($result_u_a){
						$this->output->set_status_header(200);
						echo json_encode(array('result'=>'success','msg'=>'Usuário adicionado com sucesso!','redirect'=>true,'url'=>base_url('gerenciamento/usuario')));
					}else{
						$this->output->set_status_header(200);
						echo json_encode(array('result'=>'success','msg'=>'Erro ao alterar advogado, tente novamente, caso persista entre em contato com o Administrador!'));
					}				
				}else{
					$this->output->set_status_header(200);
					echo json_encode(array('result'=>'success','msg'=>'Usuário adicionado com sucesso!','redirect'=>true,'url'=>base_url('gerenciamento/usuario')));
				}
			}else{								
				$this->output->set_status_header(500);
				echo json_encode(array('result'=>'error','msg'=>'Erro ao adicionar login, tente novamente, caso persista entre em contato com o Administrador!'));
			}
		}else{
			$this->output->set_status_header(500);
			echo json_encode(array('result'=>'error','msg'=>'Erro ao adicionar usuário, tente novamente, caso persista entre em contato com o Administrador!'));
		}

	}

	public function Update(){		
		$id_usuario = $this->uri->segment(4);
		if(!empty($id_usuario)&&is_numeric($id_usuario)){
			$usuario = $this->session->userdata('logged');
			
			if($usuario['id_usuario']!=$id_usuario){			
				$nome = $this->input->post('nome');		
				$email = $this->input->post('email');		
				$data_nascimento = $this->input->post('data_nascimento');		
				$telefone = $this->input->post('telefone');		
				$cpf = $this->input->post('cpf');		
				$senha = $this->input->post('senha')==NULL?'':md5($this->input->post('senha'));		
				$confirmar_senha = $this->input->post('senha')==NULL?'':md5($this->input->post('confirmar_senha'));		
				$perfil = $this->input->post('perfil');

				$oab = ($this->input->post('oab')!==null&&!empty($this->input->post('oab')))?$this->input->post('oab'):'';
				$rua = ($this->input->post('rua')!==null&&!empty($this->input->post('rua')))?$this->input->post('rua'):'';
				$numero = ($this->input->post('numero')!==null&&!empty($this->input->post('numero')))?$this->input->post('numero'):'';
				$bairro = ($this->input->post('bairro')!==null&&!empty($this->input->post('bairro')))?$this->input->post('bairro'):'';
				$complemento = ($this->input->post('complemento')!==null&&!empty($this->input->post('complemento')))?$this->input->post('complemento'):'';
				$cep = ($this->input->post('cep')!==null&&!empty($this->input->post('cep')))?$this->input->post('cep'):'';
				$cidade = ($this->input->post('cidade')!==null&&!empty($this->input->post('cidade')))?$this->input->post('cidade'):'';
				$estado = ($this->input->post('estado')!==null&&!empty($this->input->post('estado')))?$this->input->post('estado'):'';
				$nome_comercial = ($this->input->post('nome_comercial')!==null&&!empty($this->input->post('nome_comercial')))?$this->input->post('nome_comercial'):'';
				$descricao = ($this->input->post('descricao')!==null&&!empty($this->input->post('descricao')))?$this->input->post('descricao'):'';


				$data_u_u = array(
					'nome' => $nome,			
					'email' => $email,
					'data_nascimento' => $data_nascimento,
					'telefone' => $telefone,
					'cpf' => $cpf,			
					'ativo' => '1',			
					'deleted' => '0',			
					'createdAt' => date('Y-m-d H:i:s'),			
					'updatedAt' => date('Y-m-d H:i:s')			
				);		

				foreach ($data_u_u as $key => $value) {	
					if(empty($value)&&($value==0&&$key!='deleted')&&$value==NULL){
						$this->output->set_status_header(400);
						echo json_encode(array('result'=>'error','msg'=>'Alguns campos estão em branco!'));
						die();
					}
				}
				$this->load->model('core_model');
				$result_u_u = $this->core_model->Edit('usuario',$data_u_u,array('id_usuario'=>$id_usuario));

				if ($result_u_u) {
					if(!empty($senha)){
						if($senha!==$confirmar_senha){
							$this->output->set_status_header(400);
							echo json_encode(array('result'=>'error','msg'=>'O campo senha e confirmar senha não estão iguais!'));
							die();
						}else{
							$data_u_l = array(
								'senha' => $senha,			
								'perfil' => $perfil,		
								'last_login' => date('Y-m-d H:i:s'),			
								'updatedAt' => date('Y-m-d H:i:s')			
							);	
						}
					}else{
						$data_u_l = array(
							'perfil' => $perfil,		
							'last_login' => date('Y-m-d H:i:s'),			
							'updatedAt' => date('Y-m-d H:i:s')			
						);	
					}
					$result_u_l = $this->core_model->Edit('login',$data_u_l,array('id_usuario'=>$id_usuario));
					if($result_u_l){
						if($perfil==2){
							$data_u_a = array(
								'oab'=>$oab,
								'rua'=>$rua,
								'numero'=>$numero,
								'bairro'=>$bairro,
								'complemento'=>$complemento,
								'cep'=>$cep,
								'cidade'=>$cidade,
								'estado'=>$estado,
								'nome_comercial'=>$nome_comercial,
								'descricao'=>$descricao
							);
							$verify = $this->core_model->ListByField('advogado',array('oab'=>$oab,'id_usuario'=>$id_usuario));
							if(count($verify)>0){
								$result_u_a = $this->core_model->Edit('advogado',$data_u_a,array('id_usuario'=>$id_usuario));
							}else{
								$data_u_a['id_usuario'] = $id_usuario;
								$result_u_a = $this->core_model->Add('advogado',$data_u_a);
							}

							if($result_u_a){
								$this->output->set_status_header(200);
								echo json_encode(array('result'=>'success','msg'=>'Usuário alterado com sucesso!','redirect'=>true,'url'=>base_url('gerenciamento/usuario')));
							}else{
								$this->output->set_status_header(200);
								echo json_encode(array('result'=>'success','msg'=>'Erro ao alterar advogado, tente novamente, caso persista entre em contato com o Administrador!'));
							}				
						}else{
							$this->output->set_status_header(200);
							echo json_encode(array('result'=>'success','msg'=>'Usuário alterado com sucesso!','redirect'=>true,'url'=>base_url('gerenciamento/usuario')));
						}
					}else{								
						$this->output->set_status_header(500);
						echo json_encode(array('result'=>'error','msg'=>'Erro ao alterar login, tente novamente, caso persista entre em contato com o Administrador!'));
					}
				}else{
					$this->output->set_status_header(500);
					echo json_encode(array('result'=>'error','msg'=>'Erro ao alterar usuário, tente novamente, caso persista entre em contato com o Administrador!'));
				}
			}else{
				$this->output->set_status_header(401);
				echo json_encode(array('result'=>'error','msg'=>'Não é possível alterar seu proprio usúario! <br>Para alterar entre em Meu Perfil!'));
			}
		}else{
			$this->output->set_status_header(500);
			echo json_encode(array('result'=>'error','msg'=>'Erro ao processar sua solicitação, entre em contato com o administrador do sistema!'));
		}
	}
	public function Delete(){
		if($this->uri->segment(4)===null){
			$this->output->set_status_header(400);
			echo json_encode(array('result'=>'error','msg'=>'Erro ao deletar usuário! URL Invalida!'));
		}else{
			$id_usuario = $this->uri->segment(4);		

			$this->load->model('core_model');
			$data_d = array(
				'sha1(md5(id_usuario))' => $id_usuario,			
			);	

			$verify = $this->core_model->ListByField('usuario',$data_d);			
			if(count($verify)>0){

				$result_d = $this->core_model->Delete('usuario',$data_d);	
				if ($result_d) {
					$this->output->set_status_header(200);
					echo json_encode(array('result'=>'success','msg'=>'Usuário deletado com sucesso!'));
				}else{			
					$this->output->set_status_header(500);
					echo json_encode(array('result'=>'error','msg'=>'Erro ao deletar usuário, tente novamente, caso persista entre em contato com o Administrador!'));		
				}
			}else{
				$this->output->set_status_header(400);
				echo json_encode(array('result'=>'error','msg'=>'Erro ao deletar usuário! URL Invalida!'));
			}
		}

	}
}
