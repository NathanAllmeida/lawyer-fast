<?php
function getTipos($db){
    $db->where('deletedAt is null');            
    $db->where('deleted',0);
    $db->where('ativo',1);

    $result = $db->get('tipo');
    return $result->result_array();
}