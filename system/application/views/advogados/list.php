          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Gerenciamento de Advogados</h1>            
          </div>

          <!-- Content Row -->
          <div class="row">            
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Advogados Cadastrados</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$numbers['registered']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fa fa-user fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Acessos no último mês</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$numbers['access_last_month']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-check fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">OABs Verificadas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$numbers['oabs_verified']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-check fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">OABs Pendentes de verificação</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$numbers['oabs_pending']?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-check fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">            
            <div class="col-xl-3 col-md-6 mb-4">
              <a href="<?=base_url('gerenciamento/advogado/adicionar')?>" class="btn btn-primary"><i class="fa fa-plus"></i> Adicionar Novo Usuário</a>
            </div>
          </div>

          <!-- Content Row -->
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="list-advogados-tab" data-toggle="tab" href="#list-advogados" role="tab" aria-controls="listagem-advogados" aria-selected="true">Listagem de Advogados</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="verify-advogados-tab" data-toggle="tab" href="#verify-advogados" role="tab" aria-controls="profile" aria-selected="false">Verificação de OAB</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="list-advogados" role="tabpanel" aria-labelledby="home-tab">
              <div class="row">
                <div class="col-xl-12 col-lg-12">
                  <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Listagem de Advogados</h6>
                      <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                          <div class="dropdown-header">Dropdown Header:</div>
                          <a class="dropdown-item" href="#">Action</a>
                          <a class="dropdown-item" href="#">Another action</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                      </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                      <table class="table table-hover" id="tblLawyers">
                          <thead>
                              <tr>
                                <th></th>
                                <th>Id</th>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Nome Comercial</th>
                                <th>OAB</th>
                                <th>Situação OAB</th>                  
                                <th>Situação</th>                  
                                <th>Editar</th>
                                <th>Deletar</th>
                              </tr>                          
                          </thead>                      
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="verify-advogados" role="tabpanel" aria-labelledby="profile-tab">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                  <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">OABs Pendentes</h6>
                      <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                          <div class="dropdown-header">Dropdown Header:</div>
                          <a class="dropdown-item" href="#">Action</a>
                          <a class="dropdown-item" href="#">Another action</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                      </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                      <table class="table table-hover w-100" id="tblLawyers-verify">
                          <thead>
                              <tr>
                                <th></th>
                                <th>Id</th>
                                <th>Nome</th>                                
                                <th>Nome Comercial</th>
                                <th>OAB</th>
                                <th>Estado</th>
                                <th>Situação OAB</th>                  
                                <th>Situação</th>                  
                                <th>Aprovar</th>                                
                              </tr>                          
                          </thead>                      
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>          
          <script src="<?=base_url()?>/assets/js/script.advogado.js"></script>
      