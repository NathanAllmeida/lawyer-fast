          <!-- Page Heading -->
          <a href="<?=$url_search?>" class="btn btn-primary">Voltar</a>
          <div class="d-sm-flex align-items-center justify-content-between mb-4 mt-2  ">
            <h1 class="h3 mb-0 text-gray-800"><?=$advogado['nome_comercial']?></h1>            
          </div>
          <!-- Content Row -->

          <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
              <div class="card shadow mb-4 public-profile-lawyer">                
                <!-- Card Body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="w-100 image-search-lawyer" style='background-image:url("<?=$advogado['img_perfil']?>")'></div>
                    </div>
                    <div class="col-sm-8">
                      <p><b>Endereço:</b> <i class="fas fa-map-marker-alt"></i> <?=$advogado['endereco']?></p>
                      <p><b>Pontuação:</b> <span class='star-lawyer'><?=$advogado['score']?>  <i class="fas fa-star"></i></span> </p>
                      <p><b>Apresentação:</b> <br><?=$advogado['descricao']?></p>
                      <button class="btn btn-primary"><i class="fas fa-paper-plane"></i> Enviar Solicitação</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
