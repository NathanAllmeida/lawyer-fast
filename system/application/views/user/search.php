  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Procurar Advogado</h1>    
  </div>           
  
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade <?=$step==1?'show active':''?>" id="step-1" role="tabpanel" aria-labelledby="step-1">
      <form action="" class="row form-step-search">          
        <div class="col-xl-12 col-lg-12">
          <div class="card shadow mb-4">       
            <!-- Card Body -->
            <div class="card-body">
              <div class="container">              
                <div class="row justify-content-center align-items-center">
                  <div class="col-12 text-center">
                      <h4>Olá!<br> Seja Bem Vindo a busca de Advogados!</h4>
                      <p>Para começarmos, precisamos que você nos informe seu endereço para que possamos 
                        localizar onde você se encontra! <br>                  
                      </p>
                      <p><strong>Não se preocupe, você precisa informar apenas seu cep</strong></p>
                  </div>
                  <div class="col-2"></div>
                  <div class="col-8"> 
                    <div class="form-group text-center">
                      <label for="">Endereço</label>
                        <input type="text" class="form-control cep-mask" name='address' placeholder="Digite seu CEP" required>
                    </div>
                  </div>
                  <div class="col-2"></div>
                  <div class="col-6"> 
                    <div class="form-group text-center">
                      <button href="#step-2" class="btn btn-primary pr-5 pl-5 btn-step-search btn-step-1"><i class="fas fa-search"></i> Procurar</button>
                    </div>             
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="tab-pane fade" id="step-2" role="tabpanel" aria-labelledby="step-2">
      <form method='POST' class="row form-step-search">
        <input type="hidden" name="cep">
        <input type="hidden" name="endereco">
        <div class="col-xl-12 col-lg-12">
          <div class="card shadow mb-4">       
            <!-- Card Body -->
            <div class="card-body">
              <div class="container">              
                <div class="row justify-content-center align-items-center">
                  <div class="col-12 text-center">
                      <h4>Muito bem!</h4>
                      <h4>Você se encontra em : <strong class='search-location'></strong></h4>
                      <br>
                      <p><strong>Agora precisamos que você nos informe em qual categoria você precisa de um advogado e alguns detalhes sobre o seu problema, para que possamos encontrar o advogado perfeito!</strong></p>
                  </div>                
                  <div class="col-2"></div>
                  <div class="col-8"> 
                    <div class="form-group">
                      <label for="">Categoria</label>
                        <select name="tipo" id="" class='form-control' required>
                            <option value="" selected>Escolha uma categoria</option>
                            <?php 
                                if(count($tipos)>0){ 
                                  foreach ($tipos as $key => $value) {                                    
                            ?>
                                <option value="<?=$value['id']?>"><?=$value['nome']?></option>
                            <?php 
                                  }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label for="">Detalhes</label>
                      <textarea name="detalhes" id="" cols="30" rows="10" class='form-control' required></textarea>
                    </div>
                  </div>
                  <div class="col-2"></div>
                  <div class="col-4"></div>
                  <div class="col-6"> 
                    <div class="form-group">
                      <button class="btn btn-primary pr-5 pl-5 btn-step-search btn-step-2"><i class="fas fa-search"></i> Procurar</button>
                    </div>             
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="tab-pane fade <?=$step==3?' show active':''?>" id="step-3" role="tabpanel" aria-labelledby="step-3">
      <a href="<?=base_url('procurar-advogado')?>" class="btn btn-primary">Voltar</a>
      <form action="" class="row mt-2 form-step-search">                
        <div class="col-xl-12 col-lg-12">
          <div class="card shadow mb-4">       
            <!-- Card Body -->
            <div class="card-body">
              <div class="container">              
                <div class="row justify-content-center align-items-center">
                  <div class="col-2"></div>
                  <div class="col-8 text-left">                      
                      <h4>Sua localização: <strong><?=$localizacao?></strong> </h4>
                      <h4>Foram encontrados: <strong><?=count($advogados)?></strong> <?=count($advogados)>1?'advogados':'advogado'?> perto da sua localização</h4>
                      <br>                      
                  </div>        
                  <div class="col-2"></div> 
                  <?php if(count($advogados)>0){
                    foreach ($advogados as $key => $value) {              
                  ?>       
                    <div class="col-2"></div>
                    <div class="col-8"> 
                      <a href="<?=$value['url']?>" class="d-block no-link">
                        <div class="card card-lawyer">
                          <div class="card-body">
                            <div class="row">
                              <div class="col-sm-4">
                                <div class="w-100 h-100 image-search-lawyer" style='background-image:url("<?=$value['img_perfil']?>")'></div>
                              </div>
                              <div class="col-sm-8">
                                <p><b><?=$value['nome_comercial']?></b> <span class="verify-lawyer"  data-toggle="tooltip" data-placement="top" title="Advogado Verificado! Todos os advogados passam por uma prévia aprovação para aparecer aqui, selo de confiança Lawyer Fast ;D"><i class="fas fa-check-circle"></i></span><br> <span class='star-lawyer'><?=$value['score']?> <i class="fas fa-star"></i></span> </p><Br>
                                <p><i class="fas fa-map-marker-alt"></i> <?=$value['endereco']?></p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                    <div class="col-2"></div>
                  <?php
                    }
                  }else{
                  ?>                         
                  <div class="col-2"></div>
                  <div class="col-8"> 
                    <div class="alert alert-danger text-center">
                      <i class="fas fa-exclamation-alt"></i> Desculpe!<br> Não foi encontrado nenhum advogado perto da sua localização! Mas estamos sempre em constante evolução e crescimento!
                    </div>
                  </div>
                  <div class="col-2"></div>
                  <?php
                  }
                  ?>
                  <!-- <div class="col-4"></div>
                  <div class="col-6"> 
                    <div class="form-group">
                      <button class="btn btn-primary pr-5 pl-5 btn-step-search btn-step-2"><i class="fas fa-search"></i> Procurar</button>
                    </div>             
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?=base_url()?>/assets/js/script.search.js"></script>