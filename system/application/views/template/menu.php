        <ul class="navbar-nav mr-auto hide">
            <li class="nav-item">
                <a class="nav-link" href="#home"><i class='ti ti-home'></i> Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#about"><i class='ti ti-user'></i> Sobre mim</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#experiences"> <i class="ti ti-calendar"></i> Experiências</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#contact"><i class="ti ti-comments"></i> Contato</a>
            </li>
        </ul>