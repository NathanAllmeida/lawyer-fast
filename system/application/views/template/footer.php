

            <!-- Footer Links -->
            <div class="container-fluid text-center text-md-left">

              <!-- Grid row -->
              <div class="row">

                <!-- Grid column -->
                <div class="col-md-6 mt-md-0 mt-3">

                  <!-- Content -->
                  <h5 class="">Nathan Almeida - Desenvolvedor WEB</h5><br><br>

                  <p>"A arte de programar consiste na arte de organizar e dominar a complexidade."<small class="text-muted"> - Dijkstra</small></p>

                </div>
                <!-- Grid column -->

                <hr class="clearfix w-100 d-md-none pb-3">

                <!-- Grid column -->
                <div  style='border-left:1px solid #d3d3d3;' class="col-md-3 mb-md-0 mb-3">

                    <!-- Links -->
                    <h5 class="text-uppercase">Menu</h5>

                    <ul class="list-unstyled">
                      <li>
                        <a href="#logo-main-panel">Home</a>
                      </li>
                      <li>
                        <a href="#about">Sobre mim</a>
                      </li>
                      <li>
                        <a href="#!">Experiências</a>
                      </li>
                      <li>
                        <a href="#!">Portfolio</a>
                      </li>
                      <li>
                        <a href="#!">Contato</a>
                      </li>
                    </ul>

                  </div>
                  <!-- Grid column -->

                  <!-- Grid column -->
                  <div style='border-left:1px solid #d3d3d3;' class="col-md-3 mb-md-0 mb-3">

                    <!-- Links -->
                    <h5 class="text-uppercase">Redes Sociais</h5><br>

                    <ul class="list-unstyled">
                      <li>
                        <a class='link' href='https://www.instagram.com/allneitan/' target="__blank"><i class='ti ti-instagram'></i> Instagram</a><Br><br>
                      </li>
                      <li>
                        <a class='link' href='https://github.com/NathanAllmeida' target="__blank"><i class='ti ti-github'></i> Github</a><br><br>
                      </li>
                      <li>
                        <a class='link' href='https://www.linkedin.com/in/nathan-almeida-4591b115b/' target="__blank"><i class='ti ti-linkedin'></i> Linkedin</a><br><br>
                      </li>
                    </ul>

                  </div>
                  <!-- Grid column -->

              </div>
              <!-- Grid row -->

            </div>
            <!-- Footer Links -->

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">Desenvolvido por: Nathan Almeida - 2019 - 
              <a href="https://Allmeida.xyz"> Allmeida.xyz</a>
            </div>
            <!-- Copyright -->