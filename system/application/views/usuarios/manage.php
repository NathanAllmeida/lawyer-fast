<!-- Page Heading -->

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Gerenciamento de Usuários</h1>            
    </div>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="<?=isset($url_back)&&!empty($url_back)?$url_back:base_url('gerenciamento/usuario')?>" class="btn btn-primary">Voltar</a>            
    </div>
    
    <form action="<?= $type=='add'?base_url('gerenciamento/usuario/insert'):base_url('gerenciamento/usuario/update/'.$usuario['id_usuario']) ?>" class="form-group form-manage">
        <div class="row">
            <!-- Area Chart -->
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary"><?=$type=='add'?'Adicionar':'Editar'?> Dados do Usuário</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Dropdown Header:</div>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group"> 
                            <div class="row"> 
                                <div class="col-sm-4">
                                    <label for="">Nome</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name='nome' value='<?=count($usuario)>0?$usuario['nome']:''?>' required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="row"> 
                                <div class="col-sm-4">
                                    <label for="">Email</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="email" class="form-control" name='email' value='<?=count($usuario)>0?$usuario['email']:''?>' required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="row"> 
                                <div class="col-sm-4">
                                    <label for="">Data de Nascimento</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="date" class="form-control" name='data_nascimento' value='<?=count($usuario)>0?$usuario['data_nascimento']:''?>' required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="row"> 
                                <div class="col-sm-4">
                                    <label for="">Telefone</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control phone-mask" name='telefone' value='<?=count($usuario)>0?$usuario['telefone']:''?>' required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="row"> 
                                <div class="col-sm-4">
                                    <label for="">CPF</label>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control cpf-mask" name='cpf' value='<?=count($usuario)>0?$usuario['cpf']:''?>' required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="row"> 
                                <div class="col-sm-4">
                                    <label for="">Situação</label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="btn btn-primary active">
                                            <input type="radio" name='ativo' value='0' <?= set_radio(count($usuario)>0?$usuario['ativo']:'',0) ?>> Ativo
                                        </label>
                                        <label class="btn btn-primary">
                                            <input type="radio" name='ativo' value='1' <?= set_radio(count($usuario)>0?$usuario['ativo']:'',1) ?>> Inativo
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Edição Dados de Login</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Dropdown Header:</div>
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php if($type=='edit'){ ?>
                            <div class="form-group"> 
                                <div class="row"> 
                                    <div class="col-sm-4">
                                        <label for="">Alterar Senha?</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="btn-group btn-group-sm btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-primary">
                                                <input type="radio" name='alter-password' value='1'> Sim
                                            </label>
                                            <label class="btn btn-primary active">
                                                <input type="radio" name='alter-password' value='0'> Não
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                            <div class="alter-password <?=$type=='edit'?'d-none':''?>">
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Digite a <?=$type=='add'?'':'nova'?> Senha</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="password" class="form-control" name='senha' value='' <?=$type=='add'?'required':''?>>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Digite novamente a <?=$type=='add'?'':'nova'?> senha</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="password" class="form-control" name='confirmar_senha' <?=$type=='add'?'required':''?>>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <div class="row"> 
                                    <div class="col-sm-4">
                                        <label for="">Perfil</label>
                                    </div>
                                    <div class="col-sm-4">                                        
                                        <select name="perfil" id="" class="form-control">
                                            <option value="1" <?= (int)count($usuario)>0?(int)$usuario['perfil']==1?'selected':'':'' ?>>Usuário</option>
                                            <option value="2" <?= (int)count($usuario)>0?(int)$usuario['perfil']==2?'selected':'':'' ?>>Advogado</option>
                                            <option value="3" <?= (int)count($usuario)>0?(int)$usuario['perfil']==3?'selected':'':'' ?>>Moderador</option>
                                            <option value="4" <?= (int)count($usuario)>0?(int)$usuario['perfil']==4?'selected':'':'' ?>>Administrador</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="alter-data-lawyer <?= (int)count($usuario)>0?(int)$usuario['perfil']==2?'':'d-none':'d-none' ?>">
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">OAB</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='oab' value='<?=count($usuario)>0?$usuario['oab']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Estado</label>
                                        </div>
                                        <div class="col-sm-4">
                                        <select name="estado" class='form-control'>
                                            <option value="">Selecione um estado</option>
                                            <option value="AC"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'AC'):''?>>Acre</option>
                                            <option value="AL"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'AL'):''?>>Alagoas</option>
                                            <option value="AP"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'AP'):''?>>Amapá</option>
                                            <option value="AM"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'AM'):''?>>Amazonas</option>
                                            <option value="BA"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'BA'):''?>>Bahia</option>
                                            <option value="CE"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'CE'):''?>>Ceará</option>
                                            <option value="DF"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'DF'):''?>>Distrito Federal</option>
                                            <option value="ES"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'ES'):''?>>Espírito Santo</option>
                                            <option value="GO"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'GO'):''?>>Goiás</option>
                                            <option value="MA"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'MA'):''?>>Maranhão</option>
                                            <option value="MT"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'MT'):''?>>Mato Grosso</option>
                                            <option value="MS"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'MS'):''?>>Mato Grosso do Sul</option>
                                            <option value="MG"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'MG'):''?>>Minas Gerais</option>
                                            <option value="PA"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'PA'):''?>>Pará</option>
                                            <option value="PB"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'PB'):''?>>Paraíba</option>
                                            <option value="PR"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'PR'):''?>>Paraná</option>
                                            <option value="PE"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'PE'):''?>>Pernambuco</option>
                                            <option value="PI"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'PI'):''?>>Piauí</option>
                                            <option value="RJ"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'RJ'):''?>>Rio de Janeiro</option>
                                            <option value="RN"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'RN'):''?>>Rio Grande do Norte</option>
                                            <option value="RS"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'RS'):''?>>Rio Grande do Sul</option>
                                            <option value="RO"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'RO'):''?>>Rondônia</option>
                                            <option value="RR"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'RR'):''?>>Roraima</option>
                                            <option value="SC"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'SC'):''?>>Santa Catarina</option>
                                            <option value="SP"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'SP'):''?>>São Paulo</option>
                                            <option value="SE"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'SE'):''?>>Sergipe</option>
                                            <option value="TO"  <?= count($usuario)>0?$usuario['perfil']:''==2?set_select(count($usuario)>0?$usuario['advogado']:''['estado'],'TO'):''?>>Tocantins</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">CEP</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control cep-mask" name='cep' value='<?=count($usuario)>0?$usuario['cep']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Rua</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='rua' value='<?=count($usuario)>0?$usuario['rua']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Número</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='numero' value='<?=count($usuario)>0?$usuario['numero']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Complemento</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='complemento' value='<?=count($usuario)>0?$usuario['complemento']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Bairro</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='bairro' value='<?=count($usuario)>0?$usuario['bairro']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Cidade</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='cidade' value='<?=count($usuario)>0?$usuario['cidade']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Nome Comercial</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name='nome_comercial' value='<?=count($usuario)>0?$usuario['nome_comercial']:''?>'>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <div class="row"> 
                                        <div class="col-sm-4">
                                            <label for="">Descrição</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <textarea class="form-control" name='descricao'><?=count($usuario)>0?$usuario['descricao']:''?></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-group"> 
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-group float-right">
                                            <button class="btn btn-primary btn-save" type='submit'><i class="fas fa-check"></i> Salvar</button>
                                            <button class="btn btn-danger btn-cancel"><i class="fas fa-times"></i> Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="<?=base_url()?>/assets/js/script.usuario.js"></script>