<?php
    
    /**
     *   Login_model: A model with functions specific
     */
    class Login_model extends CI_model{

        const TABLE = 'login';

        function __construct(){
            parent::__construct();
        }

        /** 
         * SelectUserWithPass: Function that returns all data of a user 
         * @param  string $email Email of user
         * @param  string $pass Pass of user
        */
        public function SelectUserWithPass($email,$pass){
            $result = $this->db->query("SELECT * FROM usuario U INNER JOIN login L on U.id_usuario = L.id_usuario where U.email = '$email' and L.senha = '$pass' and U.deleted = 0 and U.ativo = 1");
            return $result->result_array();
        }       
    }
    
?>