<?php
    
    /**
     *   Usuario_model: A model with functions specific
     */
    class Usuario_model extends CI_model{

        const TABLE = 'usuario';

        function __construct(){
            parent::__construct();
        }

        /** 
         * SelectUserWithPass: Function that returns all data of a user 
         * @param  string $email Email of user
         * @param  string $pass Pass of user
        */
        public function SelectUserWithPass($email,$pass){
            $result = $this->db->query("SELECT * FROM Usuario U INNER JOIN Login L on U.id_usuario = L.id_usuario where U.email = '$email' and L.senha = '$pass'");
            return $result->result_array();
        }       
    }
    
?>