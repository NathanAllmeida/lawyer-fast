<?php
    
    /**
     *   Core_model: A model with a main functions for CRUD 
     */
    class Core_model extends CI_model{
        function __construct(){
            parent::__construct();
        }

        /** 
         * Get: List all lines active of a table
         * @param  string $table Table where list all lines
        */
        public function Get($table){
            $result = $this->db->get(mb_strtolower($table));
            return $result->result_array();
        } 

        /** 
         * ListAll: List all lines active of a table
         * @param  string $table Table where list all lines
        */
        public function ListAll($table,$verify = true){
            if($verify){
                $this->db->where('deletedAt is null');            
                $this->db->where('deleted',0);
                $this->db->where('ativo',1);
            }

            $result = $this->db->get(mb_strtolower($table));
            return $result->result_array();
        } 

        /** 
         * ListAllWith: List all lines active of a table with join
         * @param  string $table Table where list all lines
        */
        public function ListAllWith($table,$table_join,$primary_key,$primary_key_join,$type = 'INNER',$actived = TRUE){
            
            $this->db->where(mb_strtolower($table).'.deletedAt is null');            
            $this->db->where(mb_strtolower($table).'.deleted',0);
            if ($actived) {
                $this->db->where(mb_strtolower($table).'.ativo',1);
            }
            $this->db->join(mb_strtolower($table_join), mb_strtolower($table_join).'.'.$primary_key_join.' = '.mb_strtolower($table).'.'.$primary_key.' ',$type);
            $result = $this->db->get(mb_strtolower($table));
            return $result->result_array();
            
        } 

        /** 
         * ListAllWithMultiple: List all lines active of a table with multiple join
         * @param  string $table Table where list all lines
        */
        public function ListAllWithMultiple($tables,$primary_keys,$type = 'INNER',$actived = TRUE){
            if(count($tables)>0&&count($primary_keys)>0){
                $table_master = $tables[0];
                $primary_key_master = $primary_keys[0];
                unset($tables[0]);
                unset($primary_keys[0]);
                $this->db->where(mb_strtolower($table_master).'.deletedAt is null');            
                $this->db->where(mb_strtolower($table_master).'.deleted',0);
                if ($actived) {
                    $this->db->where(mb_strtolower($table_master).'.ativo',1);
                }
                foreach ($tables as $key => $table) {                                       
                    $this->db->join($table, $table.'.'.$primary_keys[$key].' = '.mb_strtolower($table_master).'.'.$primary_key_master.' ',$type);                    
                }
                $result = $this->db->get(mb_strtolower($table_master));
                return $result->result_array();
            }else{
                return false;
            }            
        }   

        /** 
         * ListById: List by Id lines active of a table
         * @param  string $table Table where list by id
         * @param  array $id Array Associative with name of id and your value
        */
        public function ListByField($table, $where = array(),$verify = true){
            if($verify){
                $this->db->where('deletedAt is null');            
                $this->db->where('deleted',0);
                $this->db->where('ativo',1);
            }
            foreach ($where as $key => $value) {
                $this->db->where($key,$value);
            }
            $result = $this->db->get(mb_strtolower($table));
            return $result->result_array();
        }
    
        /** 
         * Add: Add a new record in a table
         * @param  string $table Table where data to will be added
         * @param  array $data Data to be added
         * @param  array $id Array Associative with name of id and your value
        */
        public function Add($table,$data = array()){	
            $data['updatedAt'] = date('Y-m-d H:i:s');	
            $data['createdAt'] = date('Y-m-d H:i:s');	
            $result = $this->db->insert(mb_strtolower($table),$data);
            return $result;
        }
        
        /** 
         * LastId: Returns last id inserted in the database         
        */
        public function LastId(){		
            $result = $this->db->insert_id();
            return $result;
        }

        /** 
         * Edit: Edit a record in a table
         * @param  string $table Table where data to be edited
         * @param  array $data Data to be added
         * @param  array $id Array Associative with name of id and your value
        */
        public function Edit($table,$data = array(), $where = array()){
            $this->db->where($where);
            $data['updatedAt'] = date('Y-m-d H:i:s');
            $result = $this->db->update(mb_strtolower($table),$data);
            
            return $result;
        }

        /** 
         * Delete: Delete a record in a table
         * @param  string $table Table where data to be deleted
         * @param  array $id Array Associative with name of id and your value
        */
        public function Delete($table,$where = array()){		
            $this->db->where($where);
            $data = array(
                'updatedAt'=>date('Y-m-d H:i:s'),
                'deletedAt'=>date('Y-m-d H:i:s'),
                'deleted'=>1
            );
            $result = $this->db->update(mb_strtolower($table),$data);
            
            return $result;
        }

        /** 
         * Delete: Delete a record in a table
         * @param  string $table Table where data to be deleted
         * @param  array $id Array Associative with name of id and your value
        */
        public function RawQuery($sql){           
            $result = $this->db->sql($sql);
            
            return $result->result_array();
        }

    
    }
    
?>