<?php

class Login{
    private $permissions = array(
        'login_controller'=>'',
        'index_controller'=>'',
        'advogado_controller'=>'',
        'usuario_controller'=>'',
    );
    function __construct(){
        $CI =& get_instance();
    }
    public function ValidationPermissions($level = null,$controller = null, $function = null){	        
        $CI =& get_instance();  
        if ($CI->session->userdata('logged')) {            
            if($level !== null /*and $controller !== null and $function !== null*/){                          
                $perfil = (int)$CI->session->userdata('logged')['perfil'];
                if($perfil == 1 && ($level == 1 or $level == 0)){// Se o perfil for igual a usuario e o nivel de acesso for igual a usuario, permite acesso.
                    return true;
                }else if($perfil == 2 && ($level == 2 or $level == 0)){//Se o perfil for igual a advogado e o nivel de acesso for igual a advogado, permite acesso.
                    return true;
                }else if((($perfil != 1)&&($perfil != 2)) && $perfil >= $level){//Se o perfil for diferente de advogado e usuario e for maior que o nivel de acesso permite o acesso
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }		
    }
}