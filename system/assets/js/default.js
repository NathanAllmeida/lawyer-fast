$(document).ready(function(){
    $(".phone-mask").mask('(00) 00000-0000');
    $(".cpf-mask").mask('000.000.000-00');
    $(".cep-mask").mask('00000-000');
    $('[data-toggle="tooltip"]').tooltip();

    $(".form-manage").submit(function(e){
        e.preventDefault();
        let form = $(this);
        let url = $(this).attr('action');
        if(validationForm(form)){
            $.ajax({
                url: url,
                method: 'POST',
                data : form.serialize() ,
                dataType: 'json',
                beforeSend : function() {
                    
                },
                complete : function() {
                    
                },
                success: function (json) {                   
                    if(json.result=='success'){
                        Swal.fire(
                            'Sucesso!',
                            json.msg,
                            'success'
                        ).then((result) => {
                            if(json.redirect){
                                window.location = json.url;
                            }
                        });
                        
                    }else if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                },
                error: function(data){
                    console.log(data);
                    var json = data.responseJSON
                    if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                }
            });
        }
    });

    $(document).on('input','.is-invalid:not([name=senha]):not([name=confirmar_senha])',function(){
        $(this).removeClass('is-invalid');
    });
});

function validationForm(form){
    let data = form.serializeArray();
    let error = 0;
    $.each(data,function(index,field){             
        if(field.value==""||field.value==" "){
            let attr =  form.find('input[name='+field.name+'],select[name='+field.name+'],textarea[name='+field.name+']').attr('required');            
            if (typeof attr !== typeof undefined && attr !== false) {
                form.find('input[name='+field.name+'],select[name='+field.name+'],textarea[name='+field.name+']').addClass('is-invalid');
                error = 1;
            }
        }
    });  
    if(error==1){
        return false;
    }else{
        return true;
    }
}

function showLoading(that){
    $(that).addClass('button-loading');
}
function hideLoading(){}