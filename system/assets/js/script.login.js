$(document).ready(function(){
    $(".btn-user").click(function () {
        let email = $("#login-email").val();
        let pass = $("#login-pass").val();
        if(email=="" || email == " " || pass == "" || pass == " "){
            $("#login-email").addClass('is-invalid');
            $("#login-pass").addClass('is-invalid');
        }else{
            $.ajax({
                url: "login/Authentication",
                type: "POST",
                data: {
                        'email':email,
                        'pass':pass,
                      },
                beforeSend : function() {
                    Swal.showLoading();
                },
                complete : function() {
                    
                },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json.result == 'success') {
                        setTimeout(function(){
                            Swal.fire(
                                'Sucesso!',
                                'Login realizado com sucesso!',
                                'success'
                            );
                            setTimeout(function(){ window.location.href = json.url;},1000);
                        },2000);
                    }else if(json.result == 'error'){
                        Swal.disableButtons()
                        Swal.hideLoading();
                        $(".text-alert").addClass('d-flex');
                        $(".text-alert p span").text(json.msg);                        
                    }           
                }
           });
    
           /* Swal.fire(
                'The Internet?',
                'That thing is still around?',
                'question'
              )  */            
        }
    })
});