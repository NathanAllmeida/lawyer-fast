var table;
var table_verify;
$(document).ready(function(){
    table = $('#tblLawyers').DataTable({
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado.",
            "sInfo": "_TOTAL_ registros",
            "sInfoEmpty": "0 Registros",
            "sInfoFiltered": "(De _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Mostrar _MENU_ registros por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado.",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "ajax": {
            "url": 'advogado/ListAll/',
        },
        "columnDefs": [           
            {
                "targets": 9,
                "visible": true,
                "searchable": true
            },
            { "orderable": false, "targets": 0 },
            {
                "targets": [],
                "visible": false,
                "searchable": true
            },
             {
                "targets": 8,
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return "<a href='"+data.url_edit+"' class='btn btn-success btn-alterar'><i class='fas fa-edit' title='Atualizar Dados'></i></a>"
                }                
            },           
            {
                "targets": 9, 
                "data": null,                
                "render": function ( data, type, full, meta ) {
                    return "<a href='"+data.url_delete+"' class='btn btn-danger btn-delete'><i class='fas fa-trash' title='Deletar Advogado'></i></a>"
                } 
            }
        ],
        "columns": [{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": '<i class="fas fa-plus"></i>'
            },
            {"data":"id_usuario"},
            {"data": "nome"},
            {"data": "email"},
            {"data": "nome_comercial"},
            {"data": "oab"},
            {"data": "status_oab"},
            {"data": "ativo"}]
            /*"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            
            }*/
    });
    table_verify = $('#tblLawyers-verify').DataTable({
        "oLanguage": {
            "sEmptyTable": "Nenhum registro encontrado.",
            "sInfo": "_TOTAL_ registros",
            "sInfoEmpty": "0 Registros",
            "sInfoFiltered": "(De _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Mostrar _MENU_ registros por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado.",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "ajax": {
            "url": 'advogado/ListAll/pending-oabs',
        },
        "columnDefs": [           
            {
                "targets": 8,
                "visible": true,
                "searchable": true
            },
            { "orderable": false, "targets": 0 },
            {
                "targets": [],
                "visible": false,
                "searchable": true
            },
             {
                "targets": 8,
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return "<a href='"+data.url_verify+"' class='btn btn-success btn-verificar-oab'><i class='fas fa-check'></i></a>"
                }                
            },    
        ],
        "columns": [{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": '<i class="fas fa-plus"></i>'
            },
            {"data":"id_usuario"},
            {"data": "nome"},            
            {"data": "nome_comercial"},
            {"data": "oab"},
            {"data": "estado"},
            {"data": "status_oab"},
            {"data": "ativo"}]
            /*"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            
            }*/
    });

    $("input[name=alter-password]").change(function(){
        let value = $(this).val();
        if(value==1){
            $('input[name=senha]').prop('required',true);
            $('input[name=confirmar_senha]').prop('required',true);
            $('.alter-password').removeClass('d-none');
        }else{
            $('input[name=senha]').prop('required',false);
            $('input[name=confirmar_senha]').prop('required',false);
            $('.alter-password').addClass('d-none');
        }
    });
    $("select[name=perfil]").change(function(){
        let value = $(this).val();
        if(value==2){
            $('input[name=oab]').prop('required',true);
            $('select[name=estado]').prop('required',true);
            $('.alter-data-lawyer').removeClass('d-none');
        }else{
            $('input[name=oab]').prop('required',false);
            $('select[name=estado]').prop('required',false);
            $('.alter-data-lawyer').addClass('d-none');
        }
    });
    $(document).on('input','input[name=confirmar_senha]',function(){
        let pass = $('input[name=senha]').val();
        let con_pass = $(this).val();

        if(pass!==con_pass){
            $('input[name=senha]').addClass('is-invalid');
            $(this).addClass('is-invalid');
            $('input[name=senha]').tooltip({title:'O campo confirmar senha tem que ser igual ao campo senha!',placement:'right'}).tooltip('show');
        }else{
            $('input[name=senha]').removeClass('is-invalid');
            $(this).removeClass('is-invalid');
            $('input[name=senha]').tooltip('hide');
        }
    });

    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        let data = table.row( $(this).parents('tr') ).data();
        let name = data.nome;
        let url = $(this).attr('href');
        Swal.fire({
            title: 'Você quer deletar o advogado '+name+'?',
            text: "Você não vai poder reverter isso!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, delete!'
          }).then((result) => {
            $.ajax({
                url: url,
                method: 'GET',                
                dataType: 'json',
                beforeSend : function() {
                    Swal.showLoading();
                },
                complete : function() {
                    Swal.hideLoading();
                },
                success: function (json) {                   
                    if(json.result=='success'){                        
                        Swal.fire(
                            'Deletado!',
                            'O advogado foi deletado com sucesso!',
                            'success'
                        ).then((result) => {
                            table.ajax.reload();
                        });                        
                    }else if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                },
                error: function(json){                    
                    if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                }
            });         
          })
    })
    $(document).on('click','.btn-verificar-oab',function(e){
        e.preventDefault();
        let data = table_verify.row( $(this).parents('tr') ).data();
        console.log('sdadsdas',data);
        let name = data.nome;
        let url = $(this).attr('href');
        Swal.fire({
            title: 'Você quer aprovar o advogado '+name+'?',
            text: "Lembre-se de confirmar o advogado!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, aprovar!'
          }).then((result) => {
            $.ajax({
                url: url,
                method: 'GET',                
                dataType: 'json',
                beforeSend : function() {
                    Swal.showLoading();
                },
                complete : function() {
                    Swal.hideLoading();
                },
                success: function (json) {                   
                    if(json.result=='success'){                        
                        Swal.fire(
                            'Aprovado!',
                            'O advogado foi aprovado com sucesso!',
                            'success'
                        ).then((result) => {
                            table_verify.ajax.reload();
                        });                        
                    }else if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                },
                error: function(json){                    
                    if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                }
            });         
          })
    })
});