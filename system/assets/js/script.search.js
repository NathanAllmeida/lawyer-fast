var dataSearch = [];
var actualStep = 1;
$(document).ready(function(){
    $('.form-step-search').submit(function(e){
        e.preventDefault();
     });
     $(".btn-step-search.btn-step-1").click(function(event){
        showLoading(this);
        event.preventDefault();
         if(validationForm($(this).parents('form'))){
            let cep = $('input[name=address]').val().replace('-','');
            $.ajax({
                url: 'https://viacep.com.br/ws/'+cep+'/json/',
                method: 'GET',                
                dataType: 'json',
                beforeSend : function() {
                    
                },
                complete : function() {
                    
                },
                success: function (json) {                   
                    if(typeof json.erro == "undefined"){
                        $('#step-2').find('input[name=cep]').val(cep);
                        goToStep(2);
                        $(".search-location").text(json.logradouro+', '+json.bairro+', '+json.localidade+' - '+json.uf);
                        $("#step-2").find('input[name=endereco]').val(json.logradouro+', '+json.bairro+', '+json.localidade+' - '+json.uf);
                        dataSearch = json;
                    }else{                        
                        Swal.fire(
                            'Erro!',
                            'Verifique o CEP digitado!',
                            'error'
                        );
                    }
                },
                error: function(data){
                    console.log(data);
                    var json = data.responseJSON
                    if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                }
            });
         }else{
             event.stopImmediatePropagation();
         }
     });
     $(".btn-step-search.btn-step-2").click(function(event){
        showLoading(this);
        event.preventDefault();
         if(validationForm($(this).parents('form'))){
            let data = $(this).parents('form').serialize();
            $.ajax({
                url: 'gerenciamento/search/insert',
                method: 'POST',
                data: data,                
                dataType: 'json',
                beforeSend : function() {
                    
                },
                complete : function() {
                    
                },
                success: function (json) {                   
                    if(json.result == "success"){ 
                        if(json.redirect){
                            window.location.href=json.url
                        }                       
                        goToStep(3);                        
                    }else{                        
                        Swal.fire(
                            'Erro!',
                            'Erro ao buscar advogados, tente novamente mais tarde! Caso persista o erro, entre em contato com o suporte.',
                            'error'
                        );
                    }
                },
                error: function(data){
                    console.log(data);
                    var json = data.responseJSON
                    if(json.result=='error'){
                        Swal.fire(
                            'Erro!',
                            json.msg,
                            'error'
                        );
                    }
                }
            });
         }else{
             event.stopImmediatePropagation();
         }
     });
     
     $(".btn-step-search.btn-step-3").click(function(event){
        
     });
});

function goToStep(step){
    $('#step-'+actualStep).removeClass('show active');
    $('#step-'+step).addClass('show active');
    actualStep = step;
}