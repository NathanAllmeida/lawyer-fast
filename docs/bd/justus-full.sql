-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 29-Dez-2019 às 17:28
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `justus`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `advogado`
--

CREATE TABLE `advogado` (
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `oab` varchar(50) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `cidade` varchar(150) DEFAULT NULL,
  `rua` varchar(150) DEFAULT NULL,
  `numero` varchar(150) DEFAULT NULL,
  `complemento` varchar(150) DEFAULT NULL,
  `bairro` varchar(250) NOT NULL,
  `cep` varchar(250) NOT NULL,
  `lat` varchar(250) DEFAULT NULL,
  `lng` varchar(250) DEFAULT NULL,
  `nome_comercial` varchar(150) DEFAULT NULL,
  `descricao` text DEFAULT NULL,
  `status_oab` int(1) NOT NULL DEFAULT 0,
  `score` decimal(3,2) NOT NULL DEFAULT 5.00,
  `ativo` int(10) UNSIGNED DEFAULT 1,
  `deleted` int(10) UNSIGNED DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `advogado`
--

INSERT INTO `advogado` (`id_usuario`, `oab`, `estado`, `cidade`, `rua`, `numero`, `complemento`, `bairro`, `cep`, `lat`, `lng`, `nome_comercial`, `descricao`, `status_oab`, `score`, `ativo`, `deleted`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, '12345678', 'MG', '1', '1', '1', '1', '1', '32070340', NULL, '', 'Nathan Associados', '1', 0, '5.00', 1, 0, '0000-00-00 00:00:00', NULL, NULL),
(9, '0000', 'MG', 'Contagem', 'Rua Vinte e Oito', '173', 'casa', 'Tropical', '32070340', NULL, '', 'Advogados Associados', 'Somos um sociedade que procuramos a justiça acima de tudo', 0, '5.00', 1, 1, '2019-12-12 07:00:05', '2019-12-12 07:41:20', '2019-12-12 07:41:20'),
(12, '1234', 'MG', 'Contagem', 'Rua Vinte e oito', '173', 'casa', 'Tropical', '32070340', '-19.917189', '-44.1319793', 'Nathan Advogados', 'oi sou o batman', 1, '5.00', 1, 0, '2019-12-20 10:11:42', '2019-12-22 15:04:49', NULL),
(13, '12345678', 'MG', 'Contagem', 'Vinte e Oito', '173', 'casa', 'Tropical', '32070340', NULL, '', 'Advogado e Etc', '', 1, '5.00', 1, 0, '2019-12-22 14:50:06', '2019-12-22 15:07:25', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `busca`
--

CREATE TABLE `busca` (
  `id` int(11) NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `id_tipo` int(10) NOT NULL,
  `cep` varchar(50) NOT NULL,
  `endereco` varchar(250) DEFAULT NULL,
  `detalhes` text NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `busca`
--

INSERT INTO `busca` (`id`, `id_usuario`, `id_tipo`, `cep`, `endereco`, `detalhes`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 8, 2, '', '', '1234', '2019-12-29 03:40:24', '2019-12-29 03:40:24', NULL),
(2, 8, 4, '32070340', '', 'qqqqqqq', '2019-12-29 03:44:28', '2019-12-29 03:44:28', NULL),
(3, 8, 6, '32070340', '', 'E aí', '2019-12-29 15:55:24', '2019-12-29 15:55:24', NULL),
(4, 8, 12, '32070340', '', 'E aí', '2019-12-29 16:01:06', '2019-12-29 16:01:06', NULL),
(5, 8, 10, '32070340', 'Rua Vinte e Oito, Tropical, Contagem - MG', 'Colé meu bom', '2019-12-29 17:09:02', '2019-12-29 17:09:02', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

CREATE TABLE `login` (
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `senha` varchar(32) DEFAULT NULL,
  `perfil` int(5) UNSIGNED DEFAULT 1,
  `last_login` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `login`
--

INSERT INTO `login` (`id_usuario`, `senha`, `perfil`, `last_login`, `updatedAt`, `createdAt`, `deletedAt`) VALUES
(1, '81dc9bdb52d04dc20036dbd8313ed055', 4, '2019-12-26 09:30:56', '2019-12-26 09:30:56', NULL, NULL),
(2, '81dc9bdb52d04dc20036dbd8313ed055', 4, '2019-12-26 09:30:56', '2019-12-26 09:30:56', NULL, NULL),
(4, '81dc9bdb52d04dc20036dbd8313ed055', 1, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-11-30 21:24:40', NULL),
(5, '81dc9bdb52d04dc20036dbd8313ed055', 1, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-11-30 21:25:12', NULL),
(6, '81dc9bdb52d04dc20036dbd8313ed055', 1, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-11-30 21:26:10', NULL),
(7, '81dc9bdb52d04dc20036dbd8313ed055', 1, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-11-30 21:26:45', NULL),
(8, '81dc9bdb52d04dc20036dbd8313ed055', 1, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-12-10 23:19:41', NULL),
(9, '81dc9bdb52d04dc20036dbd8313ed055', 1, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-12-12 06:54:34', NULL),
(10, '81dc9bdb52d04dc20036dbd8313ed055', 2, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-12-20 10:03:06', NULL),
(11, '81dc9bdb52d04dc20036dbd8313ed055', 2, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-12-20 10:09:23', NULL),
(12, '81dc9bdb52d04dc20036dbd8313ed055', 2, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-12-20 10:11:42', NULL),
(13, '81dc9bdb52d04dc20036dbd8313ed055', 2, '2019-12-26 09:30:56', '2019-12-26 09:30:56', '2019-12-22 14:50:06', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo`
--

CREATE TABLE `tipo` (
  `id` int(10) NOT NULL,
  `nome` varchar(300) NOT NULL,
  `ativo` int(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 1,
  `deleted` int(1) UNSIGNED ZEROFILL NOT NULL DEFAULT 0,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `tipo`
--

INSERT INTO `tipo` (`id`, `nome`, `ativo`, `deleted`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 'Direito Constitucional', 1, 0, NULL, NULL, NULL),
(2, 'Direito Administrativo', 1, 0, NULL, NULL, NULL),
(3, 'Direito Tributário', 1, 0, NULL, NULL, NULL),
(4, 'Direito Empresarial', 1, 0, NULL, NULL, NULL),
(5, 'Direito Penal', 1, 0, NULL, NULL, NULL),
(6, 'Direito Civil', 1, 0, NULL, NULL, NULL),
(7, 'Direito de Família', 1, 0, NULL, NULL, NULL),
(8, 'Direito de Sucessões', 1, 0, NULL, NULL, NULL),
(9, 'Direito Eleitoral', 1, 0, NULL, NULL, NULL),
(10, 'Direito Ambiental', 1, 0, NULL, NULL, NULL),
(11, 'Direito do Consumidor', 1, 0, NULL, NULL, NULL),
(12, 'Direito Previdenciário', 1, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `nome` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `telefone` varchar(24) DEFAULT NULL,
  `cpf` varchar(11) DEFAULT NULL,
  `ativo` int(10) UNSIGNED DEFAULT 1,
  `deleted` int(10) UNSIGNED DEFAULT 0,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nome`, `email`, `data_nascimento`, `telefone`, `cpf`, `ativo`, `deleted`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 'Nathan Teodoro de Almeida', 'almeidanathan96@gmail.com', '1999-08-07', '(31) 97520-3536', '140.723.566', 1, 0, '2019-11-20 23:19:19', '2019-11-20 23:19:19', NULL),
(2, 'Alan Barbosa', 'alan@lawyerfast.com', '1000-01-01', '(11) 11111-111', '222.222.222', 1, 0, '2019-11-30 21:36:37', '2019-11-30 21:36:37', NULL),
(3, 'Teste3011', 'teste3011@gmail.com', '2019-11-30', '(31) 33978-974', '140.723.566', 1, 0, '2019-11-30 21:23:19', '2019-11-30 21:23:19', NULL),
(4, 'Teste3011', 'teste3011@gmail.com', '2019-11-30', '(31) 33978-974', '140.723.566', 1, 1, '2019-11-30 21:24:40', '2019-11-30 21:58:27', '2019-11-30 21:58:27'),
(5, 'Teste3011', 'teste3011@gmail.com', '2019-11-30', '(31) 33978-974', '140.723.566', 1, 1, '2019-11-30 21:25:12', '2019-11-30 21:59:28', '2019-11-30 21:59:28'),
(6, 'Teste3011', 'teste3011@gmail.com', '2019-11-30', '(31) 33978-974', '140.723.566', 1, 1, '2019-11-30 21:26:09', '2019-11-30 22:00:28', '2019-11-30 22:00:28'),
(7, 'Teste3011', 'teste3011@gmail.com', '2019-11-30', '(31) 33978-974', '140.723.566', 1, 1, '2019-11-30 21:26:45', '2019-11-30 22:01:02', '2019-11-30 22:01:02'),
(8, 'user', 'user@teste.com', '2019-12-10', '(31) 97520-3536', '140.723.566', 1, 0, '2019-12-10 23:19:41', '2019-12-10 23:19:41', NULL),
(9, 'Advogado', 'adv@lawyerfast.com', '2000-01-01', '(31) 33978-974', '140.723.566', 1, 0, '2019-12-12 07:00:04', '2019-12-12 07:00:04', NULL),
(10, 'Teste Advogado', 'advogado@teste.com', '2000-01-01', '(31) 33978-974', '140.723.566', 1, 1, '2019-12-20 10:03:05', '2019-12-20 10:08:21', '2019-12-20 10:08:21'),
(11, 'Teste Advogado', 'advogado@teste.com', '2000-01-01', '(31) 33978-974', '140.723.566', 1, 0, '2019-12-20 10:09:23', '2019-12-20 10:09:23', NULL),
(12, 'Advogado Teste 0610', 'Teste@advogado.com.br', '2000-01-01', '(31) 33978-974', '140.723.566', 1, 0, '2019-12-20 10:11:42', '2019-12-20 10:11:42', NULL),
(13, 'TESTE 1049', 'teste1049@email.com', '2000-01-01', '(31) 00000-0000', '140.723.566', 1, 0, '2019-12-22 14:50:05', '2019-12-22 14:50:05', NULL);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `advogado`
--
ALTER TABLE `advogado`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `advogado_FKIndex1` (`id_usuario`);

--
-- Índices para tabela `busca`
--
ALTER TABLE `busca`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuario_id` (`id_usuario`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- Índices para tabela `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `login_FKIndex1` (`id_usuario`);

--
-- Índices para tabela `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `busca`
--
ALTER TABLE `busca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `advogado`
--
ALTER TABLE `advogado`
  ADD CONSTRAINT `advogado_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `busca`
--
ALTER TABLE `busca`
  ADD CONSTRAINT `id_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tipo` (`id`),
  ADD CONSTRAINT `usuario_id` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Limitadores para a tabela `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
